<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package UniversityOfReading
 */

get_header(); ?>

<header class="sq-main-header">
	<div class="sq-container">
		<h1 class="sq-main-title"><?php esc_html_e( 'Page not accessible' ); ?></h1>
	</div>
</header><!-- .entry-header -->

<div class="content-center" id="main-content">
    <div class="cl-main_2 c-<?php echo get_theme_mod( 'color_settings');?>">
        <div class="content-body">
		<br><br><br>
			<h1>That page can&rsquo;t be found, or is not accessible.</h1>
			<br><br>
			<?php
			if ( !is_user_logged_in() ) {
				echo '<p>If you\'re trying to access a protected page, you must first <a href=\'./wp-login.php\'><b>login</b>.</a></p>';
			} 
			?>
			
		</div>
	</div>
			
</div>

<?php get_footer(); ?>
