
<div class="emdcol span_1_of_5">
    <a href="<?php echo get_permalink(); ?>" title="<?php echo esc_html(emd_mb_meta('emd_person_fname')); ?>		
		<?php echo esc_html(emd_mb_meta('emd_person_lname')); ?>">
        <div class="emdlabel outside bottom" data-label="<?php echo esc_html(emd_mb_meta('emd_person_fname')); ?> <?php echo esc_html(emd_mb_meta('emd_person_lname')); ?>"> 
		<?php if (get_post_meta($post->ID, 'emd_person_photo')) {
			$sval = get_post_meta($post->ID, 'emd_person_photo');
			$thumb = wp_get_attachment_image_src($sval[0], 'thumbnail');
			echo '<img class="emd-img thumb" src="' . $thumb[0] . '" width="' . $thumb[1] . '" height="' . $thumb[2] . '" alt="' . get_post_meta($sval[0], '_wp_attachment_image_alt', true) . '"/>';
		} ?> 
		</div>
		<?php if (emd_is_item_visible('ent_person_office', 'campus_directory', 'attribute')) { ?> 
            <div class="emdlabel  bottom"> <span class="dashicons dashicons-building"></span> <?php echo esc_html(emd_mb_meta('emd_person_office')); ?></div>
            <?php
		} ?>
		<?php if (emd_is_item_visible('ent_person_phone', 'campus_directory', 'attribute')) { ?> 
			<div class="emdlabel  bottom"> <span class="dashicons dashicons-phone"></span> <a href="tel:<?php echo esc_html(emd_mb_meta('emd_person_phone')); ?>
			"><?php echo esc_html(emd_mb_meta('emd_person_phone')); ?></a> </div>
		<?php
		} ?>
    </a>
</div>