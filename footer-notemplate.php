<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package University of Reading
 */
?>



<!-- SCRIPTS -->
<!-- Modernizr - SVG & touch -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/rdg_modernizr-svg-touch.min.js"></script>


<!-- General JS snippets --> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/rdg_javascript.js"></script>
<?php wp_footer(); ?>
</body>
</html>