<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package University of Reading
 * This theme is a copy of the Active Edition (AE) theme designed for the University of Reading. 
 * All css and javascript are copy of AE.
 */
 ?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js hover c-<?php echo get_theme_mod( 'color_settings');?>_selection">
  <head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>	<?php wp_title();?>  </title>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

<!-- STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_stylesheet.css?dr-pgr-v=1.0" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_colours.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_mediaqueries.css?dr-pgr-v=1.0" media="screen">
<noscript><link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_noscript.css" media="screen"></noscript>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_print.css" media="print">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_high-contrast.css" media="screen">

<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="rdg_ie-fixes.css" media="screen">
  <script type="text/javascript">
    // DYNAMICALLY ADD HTML5 FIXES OUTSIDE OF THE CMS
    cmsURL = window.location.href;
    if (cmsURL.indexOf("cms.rdg.ac.uk/localuserpage") <= 0) {
      var head = document.getElementsByTagName('head')[0];
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'http://html5shiv.googlecode.com/svn/trunk/html5.js';
      head.appendChild(script);
    }
  </script>
<![endif]-->

<!-- JQUERY -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<style>html.rdg_hc-stop-animation * { -webkit-transition:0 !important; transition:0 !important; }</style>


<!-- FlexSlider --> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/rdg_flexslider.min.js"></script>

<!-- VESTA -->
<link type="text/css" rel="stylesheet" href="//fast.fonts.com/cssapi/909d4c94-a678-43a8-8b47-e96df7a9882b.css"/>

<!-- VIEWPORT -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content=black>
  
<!-- FAVICON & TITLE -->
<link rel="icon" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_favicon.png" type="image/ico" />

<!-- APPLE ICONS -->
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-152-152.png"/>
<link rel="apple-touch-icon-precomposed" sizes="140x140" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-140-140.png"/>
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-76-76.png"/> 
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-57-57.png"/> 
<link rel="apple-touch-icon-precomposed" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-57-57.png"/>
<meta name="apple-mobile-web-app-title" content="Uni of Reading">    

<!-- FACEBOOK META -->
<meta property="og:image" content="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-300-300.png"/>
<meta property="og:title" content="University of Reading"/>
<meta property="og:url" content="https://www.reading.ac.uk">
<meta property="og:site_name" content="University of Reading"/> 
<meta property="og:description" content="The University of Reading is ranked in the top 1% of universities in the world. We are a global university that enjoys a world-class reputation for teaching, research and enterprise.">
<meta property="og:type" content="website"/>

<!-- MS META -->
<meta name="msapplication-TileImage" content="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-144-144.png"/>
<meta name="msapplication-TileColor" content="#d11620"/>
<meta name="application-name" content="Uni of Reading"/>

<!-- TWITTER CARD -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="UniofReading">
<meta name="twitter:title" content="University of Reading">
<meta name="twitter:description" content="The University of Reading is ranked in the top 1% of universities in the world. We are a global university that enjoys a world-class reputation for teaching, research and enterprise.">
<meta name="twitter:creator" content="UniofReading">
<meta name="twitter:image:src" content="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-300-300.png">
<meta name="twitter:domain" content="https://www.reading.ac.uk">

<?php wp_head();  ?>
</head>
    
<body>





