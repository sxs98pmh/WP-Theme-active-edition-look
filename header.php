<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package University of Reading
 * This theme is a copy of the Active Edition (AE) theme designed for the University of Reading. 
 * All css and javascript are copy of AE.
 */
 ?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js hover c-<?php echo get_theme_mod( 'color_settings');?>_selection">
  <head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>	<?php wp_title();?>  </title>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

<!-- STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_stylesheet.css?dr-pgr-v=1.0" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_colours.css" media="screen">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_mediaqueries.css?dr-pgr-v=1.0" media="screen">
<noscript><link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_noscript.css" media="screen"></noscript>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_print.css" media="print">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/rdg_high-contrast.css" media="screen">

<?php 
// STYLESHEETS. 10/10/2018: kept the above <link> tags as the following didn't work properly...
/*wp_enqueue_style('rdg_stylesheet', get_template_directory_uri() . '/css/rdg_stylesheet.css?dr-pgr-v=1.0', array(), filemtime(get_template_directory() . '/css/rdg_stylesheet.css'), 'screen');
wp_enqueue_style('style_UoR', get_template_directory_uri() . '/css/style_UoR.css', array(), filemtime(get_template_directory() . '/css/style_UoR.css'), false);
wp_enqueue_style('rdg_colours', get_template_directory_uri() . '/css/rdg_colours.css', array(), filemtime(get_template_directory() . '/css/rdg_colours.css'), false);
wp_enqueue_style('rdg_mediaqueries', get_template_directory_uri() . '/css/rdg_mediaqueries.css', array(), filemtime(get_template_directory() . '/css/rdg_mediaqueries.css'), false);
wp_enqueue_style('rdg_print', get_template_directory_uri() . '/css/rdg_print.css', array(), filemtime(get_template_directory() . '/css/rdg_print.css'), false);
wp_enqueue_style('rdg_high-contrast', get_template_directory_uri() . '/css/rdg_high-contrast.css', array(), filemtime(get_template_directory() . '/css/rdg_high-contrast.css'), false);*/

//vesta
wp_enqueue_style('vesta', '//fast.fonts.com/cssapi/909d4c94-a678-43a8-8b47-e96df7a9882b.css', array(), null, false);
?>

<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="rdg_ie-fixes.css" media="screen">
  <script type="text/javascript">
    // DYNAMICALLY ADD HTML5 FIXES OUTSIDE OF THE CMS
    cmsURL = window.location.href;
    if (cmsURL.indexOf("cms.rdg.ac.uk/localuserpage") <= 0) {
      var head = document.getElementsByTagName('head')[0];
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'http://html5shiv.googlecode.com/svn/trunk/html5.js';
      head.appendChild(script);
    }
  </script>
<![endif]-->


<style>html.rdg_hc-stop-animation * { -webkit-transition:0 !important; transition:0 !important; }</style>


<?php 
//Fix jquery version to 1.7.2
wp_deregister_script('jquery');
wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"), false, '1.7.2');
//Flexslider
wp_enqueue_script('rdg_flexslider', get_template_directory_uri() . '/js/rdg_flexslider.min.js', array('jquery'), filemtime(get_template_directory() . '/js/rdg_flexslider.min.js'), false);
?>


<!-- VIEWPORT -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content=black>
  
<!-- FAVICON & TITLE -->
<link rel="icon" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_favicon.png" type="image/ico" />

<!-- APPLE ICONS -->
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-152-152.png"/>
<link rel="apple-touch-icon-precomposed" sizes="140x140" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-140-140.png"/>
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-76-76.png"/> 
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-57-57.png"/> 
<link rel="apple-touch-icon-precomposed" href="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-57-57.png"/>
<meta name="apple-mobile-web-app-title" content="Uni of Reading">    

<!-- FACEBOOK META -->
<meta property="og:image" content="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-300-300.png"/>
<meta property="og:title" content="University of Reading"/>
<meta property="og:url" content="https://www.reading.ac.uk">
<meta property="og:site_name" content="University of Reading"/> 
<meta property="og:description" content="The University of Reading is a global university that enjoys a world-class reputation for teaching, research and enterprise.">
<meta property="og:type" content="website"/>

<!-- MS META -->
<meta name="msapplication-TileImage" content="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-144-144.png"/>
<meta name="msapplication-TileColor" content="#d11620"/>
<meta name="application-name" content="Uni of Reading"/>

<!-- TWITTER CARD -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="UniofReading">
<meta name="twitter:title" content="University of Reading">
<meta name="twitter:description" content="The University of Reading is a global university that enjoys a world-class reputation for teaching, research and enterprise.">
<meta name="twitter:creator" content="UniofReading">
<meta name="twitter:image:src" content="https://www.reading.ac.uk/web/FILES/_rdg_corporate-template-files/rdg_icon-300-300.png">
<meta name="twitter:domain" content="https://www.reading.ac.uk">

<?php wp_head();  ?>
</head>
    
<?php 
if ((get_field('rightboxtitle') != "") || (get_field('rightboxinherit') == true) || (is_active_sidebar( 'sidebar-right' )&& !is_page()) || get_post_type()=='emd_person') //If no right box has been defined, we'll use the full screen width
{
	echo "<body class=\"body menu_right c-_nav-section\">";
}
else 
{
	echo "<body class=\"body menu_no-right c-_nav-section\">";
}
?>



  <!-- ACCESSIBILITY -->
  <div class="rdg_accessibility-options">
      <div class="rdg_accessibility-center">
      <a id="rdg_skipNav" href="#main-content" title="Skip the navigation to access the main content of this page">Skip to main content</a>
      <h1>Department of Meteorology &ndash; University of Reading</h1>
      <a id="rdg_accessibilityInfo" href="https://www.reading.ac.uk/15/about/15/about-accessibility.aspx#access-keys">Show access keys</a>
<ul id="rdg_accessibility-keys">
  <h4>Access keys and links (<span id="rdg_accessibility-keysPC">PC &ndash; ALT + number</span><span>; </span><span id="rdg_accessibility-keysMac">Mac &ndash; ALT + CTRL + number</span>)</h4>
  <li><a href="https://www.reading.ac.uk/15/about/15/about-accessibility.aspx" accesskey="0">0 &ndash; <span>Accessibility information</span></a></li>
  <li><a href="https://www.reading.ac.uk/" accesskey="1">1 &ndash; <span>University homepage</span></a></li>
  <li><a href="https://www.reading.ac.uk/15/about/15/about-accessibility.aspx#high-contrast" accesskey="2" id="rdg_hc-toggle2">2 &ndash; <span>Toggle high contrast</span></a></li>
  <li><a href="https://www.reading.ac.uk/15/about/sitemap.aspx" accesskey="3">3 &ndash; <span>Sitemap</span></a></li>
  <li><a id="rdg_toggle-search" href="https://www.reading.ac.uk/search" accesskey="4">4 &ndash; <span>Search the University site</span></a></li>
  <li><a href="https://www.reading.ac.uk/15/about/15/about-faqs.aspx" accesskey="5">5 &ndash; <span>Frequently Asked questions (FAQ)</span></a></li>
  <li><a href="https://www.reading.ac.uk/15/about/15/about-contacts.aspx" accesskey="9">9 &ndash; <span>Main University contacts</span></a></li>
</ul>
    </div>
  </div>

  <!-- OPEN WRAPPERS -->
  <div class="wrapper">
    <div class="nav-overlay" id="nav-overlay"></div>
      <div class="inside-wrapper" id="inside-wrapper">
      <noscript>
  <div class="nojs-header" id="nojs-header">
    <p>
      <input class="nojs-header-close" type="radio" id="nojs-header-close" />
      <label class="nojs-header-close-label" for="nojs-header-close">Close</label>
      <span class="nojs-header-text">Looks like you've got Javascript off. <a href="#nojs-information">Find out</a> how that will affect your experience on our site.</span>
    </p>
  </div>
</noscript>
<div class="cookie-notice" id="cookie-notice">
  <div>
    <h3><span>University of Reading </span>cookie policy</span></h3>
    <p>We use cookies on <a href="/">reading.ac.uk</a> to improve your experience. You can find out more about our <a href="/15/about/about-privacy.aspx#cookies">cookie policy</a>.<span class="cookie-mobile"> By continuing to use our site you accept these terms, and are happy for us to use cookies to improve your browsing experience.</span></p>
    <span class="cookie-notice-button"><a id="cookie-notice-close" href="#">Continue<span> using the University of Reading website</span></a></span> 
  </div>
</div>
<div class="header-container" id="top">
  <div class="header-center" id="header-center">

    <!-- DEVICE -->
    <a href="https://www.reading.ac.uk" class="header-logo"><span class="header-device">University of Reading</span></a>

    <!-- SEARCH -->
    <div class="header-search-container" id="header-search">
      <form class="header-search" id="headerSearchForm" action="https://www.reading.ac.uk/search/public/search" method="get">
        <div id="sSL-options">
          <!-- GOOGLE MINI -->
          <input type="text" name="q" id="header-search-text" class="header-search-box" title="Search the University of Reading website" placeholder="Search reading.ac.uk" accesskey="4" /><input name="site" value="internal" title="Search is internal" type="hidden"><input name="output" value="xml_no_dtd" title="Output search as XML" type="hidden"><input name="client" title="Search is internal" value="internal" type="hidden"><input name="proxystylesheet" title="Search will output to rdg stylesheets" value="internal" type="hidden">
          <!-- /GOOGLE MINI -->
        </div>

        <button type="submit" id="header-search-submit" class="header-search-submit" value="search"></button>

        <script type="text/javascript">
          var searchSelectList = '<div id="sSL-search"><select id="sSL-select" title="Select the area you wish to search"><option id="sSL-0">University site</option><option id="sSL-1">Staff directory</option></select></div>';

          var sSLinputs = new Array();
          sSLinputs[0] = '<input type="text" name="q" id="header-search-text" class="header-search-box" title="Search the University of Reading website" placeholder="Search reading.ac.uk" accesskey="4" /><input name="site" value="internal" title="Search is internal" type="hidden"><input name="output" value="xml_no_dtd" title="Output search as XML" type="hidden"><input name="client" title="Search is internal" value="internal" type="hidden"><input name="proxystylesheet" title="Search will output to rdg stylesheets" value="internal" type="hidden">';
          sSLinputs[1] = '<input type="text" name="n" id="header-search-text" title="Search the University of Reading Staff Directory" class="header-search-box" placeholder="Enter first and/or last name" accesskey="4" />';

          var sSLactions = new Array();
          sSLactions[0] = 'https://www.reading.ac.uk/search/public/search';
          sSLactions[1] = 'https://www.reading.ac.uk/search/search-staff.aspx';

          $('#header-search-submit').before(searchSelectList);
          $('#header-center').addClass('sSL-multisearch-enabled');
          $("#sSL-select").change(function() {
            var sSLval = $("#sSL-select option:selected").attr('id').replace('sSL-','');
            $('#headerSearchForm').attr('action',sSLactions[sSLval]);
            $('#sSL-options').html(sSLinputs[sSLval]);
          });
        </script>
        <style>
          .sSL-multisearch-enabled .header-search-container, .sSL-multisearch-enabled .header-search { overflow:visible; }
          .sSL-multisearch-enabled .header-search-box { width:237px; }
          .sSL-multisearch-enabled .header-search-box:focus { width:300px; }
          .sSL-multisearch-enabled #sSL-search { width:150px; float:left; display: block; border: 1px solid #ddd; border-right: 0; padding:4px 5px; }
          .sSL-multisearch-enabled #sSL-select { width:100%; height:24px; font-size:1em; border:0; }
          .sSL-multisearch-enabled #sSL-options { width:auto; float:left; }

          .lt-ie8 .sSL-multisearch-enabled .header-search-box { width:188px; height:32px; line-height:32px; }
          .lt-ie8 .sSL-multisearch-enabled .header-search-submit { position:relative; float:left; width:34px; }
          .lt-ie8 .sSL-multisearch-enabled #sSL-search { width:130px; }
        </style>
      </form>
    </div>
  </div>
</div>

<!-- NAVIGATION-->
<div class="navigation-container">
  <div class="navigation-center">

    <!-- MOBILE NAVIGATION -->
    <div class="mobile-menu-buttons">
      <a id="navigation-menu" class="navigation-menu" href="#nojs-footer"><span class="navigation-menu-icon"></span><span class="mobile-menu-label">Site navigation</span></a>
      <a id="page-menu" class="page-menu"><span class="page-menu-icon"></span><span class="mobile-menu-label">Page menu</span></a>
      <a id="search-menu" class="search-menu"><span class="search-menu-icon"></span><span class="mobile-menu-label">Search</span></a>
    </div>

    <div class="navigation-main-hold" id="navigation">

        <!-- NAVIGATION MAIN LIST -->
        <ul class="navigation-main" id="navigation-main">
          <li class="c-study"><a href="https://www.reading.ac.uk/study">Study</a></li>
          <li class="c-life"><a href="https://www.reading.ac.uk/life">Life</a></li>
          <li class="c-research"><a href="https://www.reading.ac.uk/research">Research</a></li>
          <li class="c-business"><a href="http://www.reading.ac.uk/about/business.aspx">Business</a></li>
          <li class="c-about"><a href="https://www.reading.ac.uk/about">About us</a></li>
          <li class="c-news"><a href="https://www.reading.ac.uk/news-and-events">News &amp; events</a></li>
        </ul>

        <!-- SECONDARY NAVIGATION - QUICK LINKS & SOCIAL -->
        <div class="navigation-secondary">
          <ul class="navigation-submenus">
            <li>
              <!-- QUICK LINKS TOGGLE -->
              <div class="navigation-submenus-link">
                <span id="navigation-submenus-toggle" class='navigation-submenus-toggle'>
                  <span class="navigation-submenus-label">Quick links</span><span class="navigation-submenus-icon">Menu</span>
                </span>

                <!-- QUICK LINKS -->
                <ul class="navigation-submenus-sub-list arrow-list" id="navigation-submenus-list">
                  <div class="navigation-sub-section">
                    <h4><a href="https://www.reading.ac.uk/study">Study</a></h4>
<li><a href="http://discover.reading.ac.uk/study/undergraduates">Undergraduate courses</a></li>
<li><a href="http://discover.reading.ac.uk/study/postgraduates">Postgraduate courses</a></li>
                    <li><a href="https://www.reading.ac.uk/study/study-ug.aspx">Undergraduate studies</a></li>
                    <li><a href="https://www.reading.ac.uk/study/study-pg.aspx">Postgraduate studies</a></li>
                    <li><a href="https://www.reading.ac.uk/ready-to-study/international-and-eu.aspx">International students</a></li>
<li><a href="https://www.reading.ac.uk/a-z/az-academic.aspx">List of Departments</a></li>
                  </div>
                  <div class="navigation-sub-section">
                    <h4><a href="https://www.reading.ac.uk/life">Life</a></h4>
                    <li><a href="https://www.reading.ac.uk/life/life-visit.aspx">Open days</a></li>
                    <li><a href="https://www.reading.ac.uk/life/life-accommodation.aspx">Accommodation</a></li>
                    <li><a href="https://www.reading.ac.uk/ready-to-study/student-life/campus-and-facilities.aspx">Our campus</a></li>
                    <li><a href="https://www.reading.ac.uk/life/life-town.aspx">Reading town</a></li>
                  </div>
                  <div class="navigation-sub-section">
                    <h4><a href="https://www.reading.ac.uk/research">Research</a></h4>
                    <li><a href="https://www.reading.ac.uk/graduateschool/gs-home.aspx">Postgraduate Research subject areas</a></li>
                  </div>
                  <div class="navigation-sub-section">
                    <h4>Quick links</h4>
                    <li><a href="https://www.reading.ac.uk/a-z/">A&ndash;Z lists</a></li>
                    <li><a href="https://www.reading.ac.uk/library">Library</a></li>
                    <li><a href="http://henley.ac.uk">Henley Business School</a></li>
                    <li><a href="https://www.reading.ac.uk/malaysia">Malaysia Campus</a></li>
                    <li><a href="http://www.rusu.co.uk/">Students' Union</a></li>
                  </div>
                  <div class="navigation-sub-section">
                    <h4>Areas</h4>
                    <li><a href="https://www.reading.ac.uk/student">Current students</a></li>
                    <li><a href="https://www.reading.ac.uk/staff">Staff</a></li>
                    <li><a href="https://www.reading.ac.uk/alumni">Alumni</a></li>
                  </div>
                </ul>
              </div>
            </li>
          </ul>

          <!-- SOCIAL NAV -->
          <ul class="nav-soc">
            <li><a href="https://twitter.com/uniofreading" class="nav-soc-link nav-soc-twitter" title="@UniofReading on Twitter">Twitter</a></li>
            <li><a href="https://www.facebook.com/theuniversityofreading" class="nav-soc-link nav-soc-fb" title="University of Reading on Facebook">Facebook</a></li>
            <li><a href="https://www.youtube.com/user/UniofReading" class="nav-soc-link nav-soc-yt" title="University of Reading on YouTube">Youtube</a></li>
          </ul>

        </div>
      </div>

    </div>
  </div>

  <!-- BREADCRUMBS -->
  <div class="breadcrumbs-container c-<?php echo get_theme_mod( 'color_settings');?>_breadcrumbs">
    <div class="breadcrumbs-center">
      <ul class="breadcrumbs-list">
        <a class="breadcrumbs-home" href="https://www.reading.ac.uk/">Reading home</a>

			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>

      </ul>
    </div>

<script>
$(document).ready(function() {
  var rtn = (function () {
	  var i = 0;
	  return location.pathname.match(/^\/closed\/university-strategy\/WhatsHappening\/CurrentActivity\//)
	  	? function () { return (i++ === 4) ? '' : '<span class="breadcrumbs-divider">&#9656;</span>'; }
	    : function () { return '<span class="breadcrumbs-divider">&#9656;</span>'; }
	    ;
  }());

  $('.breadcrumbs-list').each(function(index) {
    var replaceArrow = $(this).html().replace(/&gt;/g, rtn).replace(/&nbsp;/g,'');
    $(this).html(replaceArrow);
  });

  // Fix Current Activites on Strategy pages
  if (location.pathname.match(/^\/closed\/university-strategy\/WhatsHappening\/CurrentActivity\//)) {
    $('.breadcrumbs-list a:nth-of-type(5)').hide();
  }

});
</script>
  </div>


<?php if ((get_field('bannertitle1') != "") || (get_field('bannerinherit') == "true"))
{ 
	$id_page=get_the_ID(); //by default, we'll display the banner for current page. But this id will be updated to parent page if inherit has been selected
	if (get_field('bannerinherit') == "true") 
	{
		$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
		foreach ($ancestors as $ancestor) //find the closest parent with a banner
		{
			if (get_field('bannertitle1',$ancestor) != "") 
			{ 
				$id_page=$ancestor; //get id of parent page
				break;
			}
		}
	}

?>
	<!-- HERO -->
  <div class='flexslider main-hero' id='main-hero-slider'>
    <ul class='slides'>
      <!-- SLIDE 1 -->
      <li id="slide1" class='main-hero-slide c-<?php echo get_theme_mod( 'color_settings');?>' style="background-image:url(' <?php $image = get_field('bannerpicture1',$id_page); echo $image['url'];?>');">
		<div class="main-hero-caption"><?php the_field("bannercaption1",$id_page);?></div>
        <div class="main-hero-overlay">
          <div class="main-hero-center">
            <div class="main-hero-table" data-href='<?php the_field("bannerlink1",$id_page);?>'>
              <div class="main-hero-headers">
                <h2 class="content-header"><?php the_field("bannertitle1",$id_page);?></h2>
                <h3 class="content-subheader"><span><?php the_field("bannertext1",$id_page);?></span></h3>
              </div>
            </div>
          </div>
        </div>
      </li>

	  <?php if (get_field("bannertitle2",$id_page) != ""){ ?> 
		  <!-- SLIDE 2 -->
		  <li id="slide2" class='main-hero-slide c-<?php echo get_theme_mod( 'color_settings');?>' style="background-image:url(' <?php $image = get_field('bannerpicture2',$id_page); echo $image['url'];?>');">
			<div class="main-hero-caption"><?php the_field("bannercaption2",$id_page);?></div>
			<div class="main-hero-overlay">
			  <div class="main-hero-center">
				<div class="main-hero-table" data-href='<?php the_field("bannerlink2",$id_page);?>'>
				  <div class="main-hero-headers">
					<h2 class="content-header"><?php the_field("bannertitle2",$id_page);?></h2>
					<h3 class="content-subheader"><span><?php the_field("bannertext2",$id_page);?></span></h3>
				  </div>
				</div>
			  </div>
			</div>
		  </li>
	  <?php }
	  
	  if (get_field("bannertitle3",$id_page) != ""){ ?> 
		  <!-- SLIDE 3 -->
		  <li id="slide3" class='main-hero-slide c-<?php echo get_theme_mod( 'color_settings');?>' style="background-image:url(' <?php $image = get_field('bannerpicture3',$id_page); echo $image['url'];?>');">
			<div class="main-hero-caption"><?php the_field("bannercaption3",$id_page);?></div>
			<div class="main-hero-overlay">
			  <div class="main-hero-center">
				<div class="main-hero-table" data-href='<?php the_field("bannerlink3",$id_page);?>'>
				  <div class="main-hero-headers">
					<h2 class="content-header"><?php the_field("bannertitle3",$id_page);?></h2>
					<h3 class="content-subheader"><span><?php the_field("bannertext3",$id_page);?></span></h3>
				  </div>
				</div>
			  </div>
			</div>
		  </li>
	  <?php }
	  
	  if (get_field("bannertitle4",$id_page) != ""){ ?> 
		  <!-- SLIDE 4 -->
		  <li id="slide4" class='main-hero-slide c-<?php echo get_theme_mod( 'color_settings');?>' style="background-image:url(' <?php $image = get_field('bannerpicture4',$id_page); echo $image['url'];?>');">
			<div class="main-hero-caption"><?php the_field("bannercaption4",$id_page);?></div>
			<div class="main-hero-overlay">
			  <div class="main-hero-center">
				<div class="main-hero-table" data-href='<?php the_field("bannerlink4",$id_page);?>'>
				  <div class="main-hero-headers">
					<h2 class="content-header"><?php the_field("bannertitle4",$id_page);?></h2>
					<h3 class="content-subheader"><span><?php the_field("bannertext4",$id_page);?></span></h3>
				  </div>
				</div>
			  </div>
			</div>
		  </li>
	  <?php } ?>

    </ul>
  </div>
	
<?php } ?>


