// JavaScript Document

// Search engine display
function search_display() {
		var searchSelectList = '<div id="sSL-search"><select id="sSL-select" title="Select the area you wish to search"><option id="sSL-0">University site</option><option id="sSL-1">Staff directory</option></select></div>';
          var sSLinputs = new Array();
          sSLinputs[0] = '<input type="text" name="q" id="header-search-text" class="header-search-box" title="Search the University of Reading website" placeholder="Search reading.ac.uk" accesskey="4" /><input name="site" value="internal" title="Search is internal" type="hidden"><input name="output" value="xml_no_dtd" title="Output search as XML" type="hidden"><input name="client" title="Search is internal" value="internal" type="hidden"><input name="proxystylesheet" title="Search will output to rdg stylesheets" value="internal" type="hidden">';
          sSLinputs[1] = '<input type="text" name="n" id="header-search-text" title="Search the University of Reading Staff Directory" class="header-search-box" placeholder="Enter first and/or last name" accesskey="4" />';

          var sSLactions = new Array();
          sSLactions[0] = '/search/public/search';
          sSLactions[1] = '/search/search-staff.aspx';

          $('#header-search-submit').before(searchSelectList);
          $('#header-center').addClass('sSL-multisearch-enabled');
          $("#sSL-select").change(function() {
            var sSLval = $("#sSL-select option:selected").attr('id').replace('sSL-','');
            $('#headerSearchForm').attr('action',sSLactions[sSLval]);
            $('#sSL-options').html(sSLinputs[sSLval]);
          });
}




// CHECK IF IE IS BEING A NUISANCE
if($.browser.msie) {
	if ($.browser.version == 11) $('html').addClass('ie11 gt-ie9');
	if ($.browser.version == 10) $('html').addClass('ie10 gt-ie9');
 	if ($.browser.version == 9) $('html').addClass('ie9 gt-ie9 lt-ie10 no-svg').removeClass('svg');
 	if ($.browser.version < 9) $('html').addClass('ie8 lt-ie9').removeClass('ie9');
 	if ($.browser.version < 8) $('html').addClass('ie7 lt-ie8').removeClass('ie8');
 	if ($.browser.version < 7) $('html').addClass('ie6 lt-ie7').removeClass('ie7');
}
else {
  $('html').addClass('not-ie');
}

// CHECK IF CMS IN EDIT
$(document).ready(function() {
  cmsURL = window.location.href;
    if (cmsURL.indexOf("cms.rdg.ac.uk/localuserpage") > 0) {
      $('html, body').addClass('rdg_CMSedit');
    }
});

// MODERNIZR TESTS
if (Modernizr.touch) { $('html').removeClass('hover'); }
if (navigator.userAgent.match(/firefox/i)) {
	if (!$('html').hasClass('firefox')) { $('html').addClass('firefox'); }
}
var OSName="Unknown";
if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
$('html').addClass('rdg_OS-'+OSName);

// ACCESSIBILITY
cmsURL = window.location.href;
$(document).ready(function() {
  var accessibilityCookie = accessibilityGetCookie("rdg_accessibilityCookie");
  if (accessibilityCookie == 'high-contrast') { $('html').addClass('rdg_hc-enabled'); }

  $("#header-center").addClass('rdg_hc-toggle-exists').append("<a href='#' title='Switch between high contrast and regular colours' class='rdg_hc-toggle' id='rdg_hc-toggle'><span>Switch to high contrast colours</span></a>");

  $('#rdg_hc-toggle, #rdg_hc-toggle2').click(function(e) {
  	e.preventDefault();
    $('html').addClass('rdg_hc-stop-animation').toggleClass('rdg_hc-enabled');
    if ($('html').hasClass('rdg_hc-enabled')) { accessibilitySetCookie('rdg_accessibilityCookie','high-contrast',33); }
    else { accessibilitySetCookie('rdg_accessibilityCookie','regular',33); }
    setTimeout(function() { $('html').removeClass('rdg_hc-stop-animation'); },300);
  });
  function accessibilityGetCookie(name) {
    var dc = document.cookie; var prefix = name + "="; var begin = dc.indexOf("; " + prefix);
    if (begin == -1) { begin = dc.indexOf(prefix); if (begin != 0) return null; }
    else {
      begin += 2; var end = document.cookie.indexOf(";", begin);
      if (end == -1) { end = dc.length; }
    }
    return unescape(dc.substring(begin + prefix.length, end));
  }
  function accessibilitySetCookie(c_name,value,expiredays) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expiredays);
    document.cookie=c_name+ "=" +escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
  }

  $('#rdg_toggle-search').click(function(e) {
  	e.preventDefault();
  	$('#header-search-text').focus();
  });
  $('#rdg_accessibilityInfo').click(function(e) {
  	e.preventDefault();
  	$('#rdg_accessibility-keys').toggle();
  	var showHide = 'Show';
  	if ($('#rdg_accessibility-keys').css("display") == "block") {
  		var showHide = 'Hide';
  	}
  	$('#rdg_accessibilityInfo').html(showHide+' access keys');
  });
});

$(document).ready(function() {

	$(".scroll").click(function(e){
    e.preventDefault();
		var s = '#'+this.href.split("#").pop(), position = $(s).offset(), t = 2000, o = 25;
    $('html, body').animate({scrollTop:position.top-o}, t);
  });


	if ($("#main-hero-slider").length != 0) {
		s1Content = $("#main-hero-slider #slide1 .content-header").html();
		s2Content = $("#main-hero-slider #slide2 .content-header").html();
		s3Content = $("#main-hero-slider #slide3 .content-header").html();
		s4Content = $("#main-hero-slider #slide4 .content-header").html();
		if (!s1Content) { $("#main-hero-slider #slide1").remove(); }
		if (!s2Content) { $("#main-hero-slider #slide2").remove(); }
		if (!s3Content) { $("#main-hero-slider #slide3").remove(); }
		if (!s4Content) { $("#main-hero-slider #slide4").remove(); }
    var shouldWePause = true;
    if ($('.slide-has-video').length <= 0) {
      var shouldWePause = false;
    }
		$("#main-hero-slider").flexslider({
			animation: "fade", slideshow:true, slideshowSpeed: 9000, multipleKeyboard:true, pauseOnAction:true, pauseOnHover:false, keyboard:true,
			start: function(slider) {
		    $('.not-ie .flex-active-slide .main-hero-overlay').css({'left':'-10px','opacity':1});
		  },
		  before: function(slider) {
		    $('.not-ie .flex-active-slide .main-hero-overlay').attr('style','');
        if ((shouldWePause) && (player && player.hasOwnProperty('pauseVideo') && typeof player.pauseVideo === "function") && (!$('html').hasClass('lt-ie9'))) {
          player.pauseVideo();
        }
		  },
			after: function(slider) {
		    $('.not-ie .flex-active-slide .main-hero-overlay').css({'left':'-10px','opacity':1});
		  }
		});
		if (!$(".flex-active-slide").length) {
		    $('.not-ie .main-hero-overlay').css({'left':'-10px','opacity':1}); //Show overlay for single slide hero
		}
		$(".main-hero-table").each(function(index){
			var href = $(this).attr('data-href');
			if (href) {
				$(this).wrap(function() {
					return "<a class='main-hero-link' href='" + href + "'></a>";
				});
			}
		});
	}


	var count = $("#myList").children().length;
	if ($('body').hasClass('js-lb')) {
		$('.lightbox-trigger').click(function(e) {
			e.preventDefault(); // prevent link
		/* POPULATE DIV */
			var ml = '#'+$(this).attr('id').replace('-trigger','');
			if (!$(ml).hasClass('lb-open')) {
				$(ml).show();
				setTimeout(function() { $(ml).addClass('lb-open'); },10);
			}
			if (!$('html').hasClass('noscroll')) {
				$('html').addClass('noscroll')
			}
		});
		$('.lightbox-hide, .lightbox-back').click(function() { closeLightbox(); });

		/* CLOSE LIGHTBOX */
		function closeLightbox() {
			$('html').removeClass('noscroll').attr('style',''); 					// remove scroll block + top set by jQuery
			$('.lightbox-hold').removeClass('lb-open');						// close lightbox
			setTimeout(function() { $('.lightbox-hold').hide(); },310);			// hide lightbox after faded out
		}
	}
});

  /////////////////////
 // Mobile nav open //
/////////////////////

$(document).ready(function() {
	$('#navigation-menu, .nav-overlay, .nav-mob-close, #search-menu').click(function(e) {
		e.preventDefault();
		$('#navigation').toggleClass('navigation-open');
		$('#inside-wrapper').toggleClass('nav-wrapper-open');
		$('#nav-overlay').toggleClass('nav-overlay-open');
	});
	// Page Menu dropdown
	$('#page-menu').click(function() {
		var pm = '#page-menu-hold', pmo = "page-menu-open";
		if ($(pm).hasClass(pmo)) {
			$(pm).animate({height: 0}, 300); setTimeout(function() { $(pm).removeClass(pmo).attr('style',''); },310);
		}
		else {
			expandDown(pm,pmo,300);
			if ($('.main-hero').length) {
				var t = $(pm).offset().top, h = $(window).height();
				if (t+200 >= h) {
					setTimeout(function() { $(".wrapper").animate({ scrollTop: t-20 }, 300); },0);
				}
			}
		}
	});
});



$(document).ready(function()  {
	$('#navigation-submenus-toggle').click(function() {
		$(this).toggleClass('nav-sub-toggle-button-open');
		$('#navigation-submenus-list').toggleClass('nav-sub-toggle-open');
	})
	// $('#navigation-submenus-list').hover(
	// 	function() {
 //  			window.clearTimeout(submenusTimer);
	// 	},
	// 	function() {
	// 		if ($(this).hasClass('nav-sub-toggle-open')) {
	// 			submenusTimer = window.setTimeout(function() {
	// 				$('#navigation-submenus-toggle').toggleClass('open');
	// 				$('#navigation-submenus-list').toggleClass('nav-sub-toggle-open');
	// 			}, 2000);
	// 		}
	// });
})


  /////////////////////
 // tabbed sections //
/////////////////////

$(document).ready(function() {

	/* LOAD IMAGES */
	setTimeout(function() { loadImages(); }, 500);
	function loadImages() {
		if (window.innerWidth >= 768) {
			$('.load-image').each(function(i,obj) {
				var that = this;
				setTimeout(function () {
					if (!$(that).hasClass('loaded-image')) {
						var src = $(that).data('src');
						setTimeout(function() {
							$(that).fadeTo(0,0).attr('src',src).fadeTo(300,1).removeClass('load-image').addClass('loaded-image');
						},300);
					}
				}, i*50);
			});
		}
	}
			

// REMOVE INTERNAL LINK FROM BREADCRUMBS
  var internalBreadcrumb = '.breadcrumbs-list a:contains(Internal Audience but Open Access)';
  $(internalBreadcrumb + ', ' + internalBreadcrumb + ' + .breadcrumbs-divider').remove();
  $('.breadcrumbs-list a:contains(Internal Audience and Closed)').attr('style','text-decoration:none !important').text('Closed').removeAttr('href');

						   
	$(window).resize(function() {
		breadCrumbsWidth();
		// Throttle resize events
    		clearTimeout($.data(this, 'resizeTimer'));
		$.data(this, 'resizeTimer', setTimeout(function() {
			setTimeout(function() {
				setTabWaypoints();
				resetTwttr('resize');
				
				$('.table-container table').each(function(i) {
			  		var tableWidth = $(this).innerWidth(), tableHolderWidth = $(this).parent('.table-holder').innerWidth();
			  		if (tableWidth > tableHolderWidth) {
			  			$(this).parent('.table-holder').prev('.table-navigation').show();
			 		}
			 		else {
			 			$(this).parent('.table-holder').prev('.table-navigation').hide();
			 		}
			  	});
			},100);
		}, 200));
	});
	
	function breadCrumbsWidth() {
		if ($('.breadcrumbs-container').length != '0') {
			bcc = 'breadcrumbs-center';
			winWid = $('.breadcrumbs-center').width() +1,	
			bcWid = $('.breadcrumbs-list').width();
			if (bcWid > winWid) { if (!$('.'+bcc).hasClass(bcc+'-ellipsis')) { $('.'+bcc).addClass(bcc+'-ellipsis'); } }
			else { $('.'+bcc).removeClass(bcc+'-ellipsis'); }
		}
	}
	breadCrumbsWidth();
	if (($('body').hasClass('body-study')) || ($('body').hasClass('body-life'))) {
		// set location
		var loc = 'courses'; if ($('body').hasClass('body-life')) { var loc = 'campusreading'; }
		// get hash & manipulate certain variants
		var hash = window.location.hash.replace('#','').replace('undergraduate','undergrad').replace('postgraduate','postgrad');
		// valid hash array
		var valid_hash = ['postgrad','undergrad','international','reading','campus','studentsupport'];
		// check if hash valid, if so execute
		if ($.inArray(hash, valid_hash) > -1) {
			tabsOpen('#'+loc+'_'+hash+'-tab');
			setTimeout(function() {
				$("html, body").animate({ scrollTop: $('#tabs_'+loc).offset().top }, 300);
			},500);
		}
	}
	function tabsOpen(that) {
		var con = 'tab-sel', opn = 'tabs-section-open';
		var curTab = '#'+$('.'+con).attr('id');
		var newTab = '#'+$(that).attr('id');
		var root = curTab.split('_')[0];
	/* Fade out container (opacity) */
		$(root+'_opacity').fadeTo(300,0);
		setTimeout(function() {
			$(root+'_opacity').fadeTo(300,1);
		},400);
		setTimeout(function() {
	/* Remove current */
			$(curTab).removeClass(con);
			$(curTab.replace('-tab','-hold')).removeClass(opn);
	/* Add new */		
			$(newTab).addClass(con);
			$(newTab.replace('-tab','-hold')).addClass(opn);
		},310);
	/* ScrollTop */
		if ($(root+'_clear').length) {
			$("html, body").animate({ scrollTop: $(root+'_clear').offset().top }, 300);
		}
	}
	if ($(".tabs-holder").length) {	
		$('.tabs-holder ul li a').click(function() {
			if (!$(this).hasClass('tab-sel')) {
				/* Set variables */
				tabsOpen(this)
			}
		});
		$('.tabs-accordian a').click(function() {
			/* Set variables */
			var curAcc = '#'+$(this).attr('id');
			var opnAcc = 'tabs-accordian-open';
			var opnAccTitle = 'tabs-accordian-title-open';
			var n = curAcc.replace('-accordian','-hold');
			/* Open */
			if (!$(curAcc).hasClass('tabs-accordian-title-open')) {
				var	curHeight = $(n).height(), autoHeight = $(n).css('height', 'auto').height();
				$(n).height(curHeight).animate({height: autoHeight}, 300);
				setTimeout(function() { $(n).removeAttr('style'); },310);
				$(curAcc).addClass(opnAccTitle);
				$(curAcc.replace('-accordian','-hold')).addClass(opnAcc);
			}
			/* Close */
			else {
				$(curAcc).removeClass(opnAccTitle);
				$(n).height(curHeight).animate({height: 0}, 300);
				setTimeout(function() { $(n).removeClass('tabs-accordian-open').removeAttr('style'); },310);
			}
			
		});
		setTabWaypoints();
	}
});

function setTabWaypoints() {
	if (($(".tabs-holder").length) && (!$(".tabs-holder").hasClass('tabs-modules'))) {	
		$(".tabs-holder").waypoint(function(direction) {
			var cid = '#'+$(this).attr('id');
			if (direction == 'down') {
				if (!$(cid).hasClass('tabs-stuck')) {
					$(cid).addClass('tabs-stuck');
				}
			}
			else {
				$(cid).removeClass('tabs-stuck');
			}
		}, { offset: 0 });
	}
}

  ///////////////////////////
 // modernizr SVG replace //
///////////////////////////


$(document).ready(function() {
	if (($('.svg-image').length) && (!Modernizr.svg)) {
		$('.svg-image').each(function(i) {
			var src = $(this).attr("src").replace('.svg','.png');
			$(this).attr("src", src);				  
		});
	}
});

/* EXPAND */

function expandDown(id,open_class,time) {
	var	c = $(id).height(), a = $(id).css('height', 'auto').height();
	$(id).height(c).animate({height: a}, time);
	setTimeout(function() { $(id).addClass(open_class).removeAttr('style'); },time+20);
}


twtRst = 'n', twtNum = 0;
function resetTwttr(change) {
	if ((twtRst != 'y') || (change == 'resize')) {
		setTimeout(function() {
			if ( $('[id*="twitter-widget"]').length ) {
				$('[id*="twitter"]').each( function(){
					var ibody = $(this).contents().find( 'body' );
					if ( ibody.find( '.timeline .stream .h-feed li.tweet' ).length ) {
						twtRst='y';
						if (change == 'reset') {
							ibody.find('.footer').hide();
							ibody.find('.tweet').css('min-height','50px');
						}
						var twtH = ibody.find('.stream').height();
						$(this).height(twtH);
					}
				});
			}
			if (change != 'resize') {
				twtNum++;
				if (twtNum < 10) { resetTwttr('reset'); }
			}
		},200);
	}
}
resetTwttr('reset');


//Format Kicker & Pullout box
$(document).ready(function() {
  $('.breadcrumbs-divider').fadeTo(0,0.5);
  $(".pullout-box h3:empty, .pullout-image:empty, .kicker-box h3:empty").remove();
  $('.pullout-box, .kicker-box').each(function(i, obj) {
    checkEmpty = $(this).html().replace(/\s+/g,'');
    if ($(this).is(':empty') || checkEmpty == '' || checkEmpty == '<p>&nbsp;</p>') { $(this).hide(); }
  });

  // Move kicker on resize
  $(window).resize(function() { kickerMove(); })
  kickerMove();
  function kickerMove() {
  	var ksm = '#kicker-seemore-box', pmh = '#page-menu-hold', rph = '#right-pullout-switch';
  	if ($(window).width() < 768  && $(pmh+' '+ksm).length) { $(ksm).appendTo(rph); }
  	if ($(window).width() >= 768 && $(rph+' '+ksm).length) { $(ksm).appendTo(pmh); }
  }

  // Format pullout with image above
  if (!cmsCheck()) {
	$('.pullout-image:has(img)').next().addClass('pullout-box-notrounded'); // unround pullout with image above
    $('.pullout-image p:has(img) + p').parent('.pullout-image').next().removeClass('pullout-box-notrounded'); //reround pullout with p between
	$('.pullout-image p:has(img) + p').prev().find('img').addClass('pullout-image-rounded'); //round image with p between
  }

  // add span to menu
  $('#ae_menu li a').each(function(i) {
  	$(this).wrapInner('<span></span>');
  });

  // add .table-head to td cells with h4 in
  if (!cmsCheck()) {
  	$(".content-body table").wrap('<div class="table-container"><div class="table-holder"></div></div>');
  	var i = 1;
  	$('.table-container').each(function(i) {
  		$(this).attr('id','rdg_table-'+i); i++;
  	});
  	$(".table-container").prepend('<div class="table-navigation"><a href="#" class="table-nav-prev"><span>Prev</span></a><a href="#" class="table-nav-next"><span>Next</span></a></div>')
  	$(".content-body table h4").each(function(i) {
  		$(this).parent().addClass('table-head');
  	});
  	$('.table-container table').each(function(i) {
  		var tableWidth = $(this).innerWidth(), tableHolderWidth = $(this).parent('.table-holder').innerWidth();
  		if (tableWidth > tableHolderWidth) {
  			$(this).parent('.table-holder').prev('.table-navigation').show();
 		}
 		else {
 			$(this).parent('.table-holder').prev('.table-navigation').hide();
 		}
  	});
  } 
  $('.table-nav-prev, .table-nav-next').click(function(e) {
  	e.preventDefault();
  	var parentScroll = $(this).parent().next('.table-holder').scrollLeft();
  	var parentScrolled = parentScroll + 200;
  	if ($(this).hasClass('table-nav-prev')) { var parentScrolled = parentScroll - 200; }	
  	$(this).parent().next('.table-holder').animate({scrollLeft: parentScrolled}, 300);
  });

  // add <wbr> to emails
  if (!cmsCheck()) {
  	$(".pullout-box a:contains('@reading.ac.uk'), .highlight-box a:contains('@reading.ac.uk')").each(function(i) {
  		var emailAd = $(this).html().replace('@reading.ac.uk','@<wbr>reading.ac.uk');
  		$(this).html(emailAd);
  	});
  } 

  // cmsCheck function
  function cmsCheck() {
  	cmsVar = false;
  	if ($('#pastetarget').length) { cmsVar = true; }
  	return cmsVar;
  }

   // Form style
   	function isEmpty(el) { return !$.trim(el.html()) }
   	$('.FormDescription').each(function() {
   		if (isEmpty($(this))) { $(this).hide(); }
  	});

	$('.content-body form input[type="text"]').each(function() {
		var inputSize = $(this).attr('size') * 5;
		if (inputSize >= 100) { inputClass = 'inputText100'; }
		else { inputClass = 'inputText'+inputSize; }
		$(this).addClass(inputClass);
	});
	$('.content-body form .infoText:contains("*"), .content-body form label:contains("*")').each(function() {
		$(this).html($(this).html().replace('*','<span class="formStarReq">*</span>'));
	});
});

	function WSRequest(strCode, strYear) {
	  var http;
	  var strURL = "http://www.reading.ac.uk/custompages/corporate_site_controls/UoR-UGCourseModDescRequest.aspx?code=" + strCode + "&year=" + strYear;
	  //var strURL = "ContentRequest.aspx?code=" + strCode + "&year=" + strYear;
	  var objDiv = document.getElementById("divModDesc");
	  if (objDiv)
	  { objDiv.innerHTML ="";
	    if (window.XMLHttpRequest) { http = new XMLHttpRequest(); } else { http = new ActiveXObject("Microsoft.XMLHTTP"); }
	    http.onreadystatechange = function() 
	    { if (http.readyState == 4)
	      { if (http.status == 200) { objDiv.innerHTML = http.responseText; openLightbox(); }
	      }
	    }
	    http.open("GET", strURL, true);
	    http.send();
	  }
	  return false;
	}
	function openLightbox() {
		var ml = '#lb-modules';
			if (!$(ml).hasClass('lb-open')) {
				$(ml).show(); setTimeout(function() { $(ml).addClass('lb-open'); },10);
			}
			if (!$('html').hasClass('noscroll')) { $('html').addClass('noscroll') }
				
			$('#MODCAT-HED h2').addClass('lightbox-module-title c-study'); // add class to h2
			$('#divModDesc').prepend($('#MODCAT-HED h2')); // move h2 to top
			$('#divModDesc table').wrap('<div class="lightbox-module-table-hold" />') // add table holds to tables
		/* CHECK TABLES and RESTYLE */
			var i = 0, e = 0, emptyRow = 0;
			$("#MODCAT-TABLE table tr").each(function(index){
				var h = $(this).html().replace(/\s+\s/g,''),  t = '<td class="data1" width="25%">&nbsp;</td><td class="data1" width="25%">&nbsp;</td><td class="data1" width="25%">&nbsp;</td><td class="data1" width="25%">&nbsp;</td>';
				if (emptyRow == 1) { e++; if (e == 1) { $(this).addClass('lb-table-bordertop'); } else if (e == 2) { $(this).addClass('lb-table-last'); } } emptyRow = 0; if (t == h) { $(this).remove(); emptyRow = 1; }
			});
		}



$(window).scroll(function() {
	// Throttle resize events
	clearTimeout($.data(this, 'scrollTimer'));
	$.data(this, 'scrollTimer', setTimeout(function() {
	    setTimeout(function() {
	    	var i = 0;
	    	$('.table-container').each(function(i) {
	    		var tableID = '#' + $(this).attr('id');
	    		if ($(this).height() > 400) {
			    	var offsetTopH = $(tableID+' .table-navigation a').offset();
			    	var marginOffset = $(window).scrollTop() - offsetTopH.top + 150;
			    	var tableHeight = $(this).height() - 150;
			    	if (marginOffset >= tableHeight) { var marginOffset = tableHeight; }
			    	if (marginOffset < 150) { var marginOffset = 150; }
			    	$(tableID+' .table-navigation a span').css('top',marginOffset+'px');
			    }
			    else {
			    	$(tableID+' .table-navigation a span').attr('style','');
			    }
			});
	    },100); 
	}, 200));
});


$(document).ready(function() {
  $(".c-news .content-body a:contains('>>>')").each(function() {
    $(this).text($(this).text().replace(' >>>',''));
    $(this).parent().addClass('make-link-button-next');
  });
});

/*
// Nice try, Rob Sterlini...
var kkeys=[],knmi="38,38,40,40,37,39,37,39,66,65";$(document).keydown(function(e){kkeys.push(e.keyCode);if(kkeys.toString().indexOf(knmi)>=0){$(document).unbind("keydown",arguments.callee);var t=1;$("body").append('<div id="rdg_pty-rve"><style>#rdg_pty-rve{background:url(http://i1.kym-cdn.com/photos/images/facebook/000/581/296/c09.jpg) center no-repeat;background-size:cover;position:fixed;top:0;left:0;height:100%;line-height:50px;margin-top:-25px;opacity:0;color:#fff;font-family:"Comic Sans MS", sans-serif;font-size:3em;font-weight:600;width:100%;z-index:90000;text-align:center;}#rdg_pty-rve span{position:absolute;left:0;width:100%;top:50%;padding:10px 20px;margin:-25px auto 0;text-shadow:rgba(0,0,0,0.5) 0 0 5px;height:50px;}</style><span>Very konami! Much code! Such party rave!</span></div>');$("html").addClass("rdg_hc-stop-animation");$('#rdg_pty-rve').fadeTo(2500,1);var n=setInterval(function(){$("html").toggleClass("rdg_hc-enabled");t++;if(t>=41){clearInterval(n);$("html").removeClass("rdg_hc-stop-animation");$("#rdg_pty-rve").remove()}},100)}});
*/

// STAFF PROFILE - remove empty sections
$(document).ready(function() {
  cmsURL = window.location.href;
  if (cmsURL.indexOf("cms.rdg.ac.uk/localuserpage") <= 0) {
    $('#staff-responsibilities, #staff-areas-of-interest, #staff-research-groups, #staff-publications').each(function(i, obj) {
      checkEmpty = $(this).html().replace(/\s+/g,'');
      if ($(this).is(':empty') || checkEmpty == '' || checkEmpty == '<p>&nbsp;</p>') {
        var emptyId = $(this).attr('id').replace('staff-','');
        $('#'+emptyId).hide();
      }
    });
  }
});

$(document).ready(function () {
	$(".refsubprofiles td").each(function (i, v) {
		v.innerHTML = isNaN(parseFloat(v.innerHTML))
			? v.innerHTML
			: Math.round(parseFloat(v.innerHTML)) + "%";
	});
});