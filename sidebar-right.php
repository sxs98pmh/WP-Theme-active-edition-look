<?php
/**
 * The right sidebar for our theme
 *
 * Displays the selected widgets
 *
 * @package University of Reading
 */
 ?>


	<?php if ( is_active_sidebar( 'sidebar-right' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-right' ); ?>
	<?php endif; ?>
	
	
